# **Description**

gives the Nuke user a warning/feedback in case the BBox of the current viewer is too large
which could cause long render times or render issues/fail

## **Installation**

- copy/paste menu.py into your Nuke home directory (Windows: drive letter:\Users\login name\.nuke, make sure the folder is not hidden) 
  or paste content into existing menu.py 
- copy/paste BBox_Checker.py into Nuke home directory or in any other location
- change folder (Nukescripts variable) in menu.py to append sys module with correct location of BBox_Checker.py