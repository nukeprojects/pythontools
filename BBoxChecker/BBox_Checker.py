# -*- coding: utf-8 -*-
#
# ------------------------------------------
# Author: Martin Fischer
# Date: 05/08/2020
#

# -----------------------------------
# ---> BBox Checker v1.01 <---
# -----------------------------------
# -------------------------------------------------------------------------------------------------------------------------
#                                                   Description:
#                       gives the Nuke user a warning/feedback in case the BBox of the current viewer is too large
#                               which could cause long render times or render issues/fail
# -------------------------------------------------------------------------------------------------------------------------


import nuke
import nukescripts
from pprint import pprint
from PySide2 import QtGui, QtCore, QtWidgets

global _BBOX_CHECKER
if '_BBOX_CHECKER' not in globals():
    _BBOX_CHECKER = {}


# create warningBox widget ----> to show user a warningBox

class warningBox(QtWidgets.QTextEdit):
    def __init__(self, parent=None):
        super(warningBox, self).__init__(parent)
        self.setMaximumHeight(30)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setStyleSheet('background-color:rgb(255,0,0)')


class bboxChecker():
    _VIEWER_WIDGET_CACHE = {}

    # FUNCTION
    # INIT
    # --------------------------------------------------------------------------------------
    def __init__(self, viewer):
        try:
            self._viewer = viewer
            self._viewerName = viewer.name()
        except:
            self._VIEWER_WIDGET_CACHE.clear()
        if self.getbbox() != None:
            self._parentToViewerWidget(viewer)


    # FUNCTION
    # GET BBOX OF ACTIVE VIEWER NODE ----> TO FIND OUT BBOX MEASURE
    # --------------------------------------------------------------------------------------
    def getbbox(self, viewer=None):
        try:
            viewer = viewer or nuke.activeViewer().node()
            active = nuke.activeViewer().activeInput()
            activeNode = viewer.input(active)
            return activeNode
        except:
            return

    # FUNCTION
    # FIND INDICATED VIEWER WIDGET ----> FOR ADDING WARNING BOX
    # --------------------------------------------------------------------------------------
    def findwidget(self):
        stack = QtWidgets.QApplication.topLevelWidgets()
        while stack:
            widget = stack.pop()
            stack.extend(c for c in widget.children() if c.isWidgetType())
            if widget.objectName().startswith('Viewer'):
                return widget

    # FUNCTION
    # ADD WARNING WIDGET INTO VIEWER WIDGET LAYOUT
    # --------------------------------------------------------------------------------------
    def _parentToViewerWidget(self, viewer):
        # print(self._VIEWER_WIDGET_CACHE)
        try:
            viewerName = viewer.name()
        except:
            return

        # if viewer not found in cache dictionary ----> find viewer widget/layout and add into cache dictionary
        if viewerName not in self._VIEWER_WIDGET_CACHE:
            viewerWidget = self.findwidget()
            widget = warningBox('BBox WARNING!! ---> reduce size to avoid render/farm issues')
            layout = viewerWidget.layout()
            self._VIEWER_WIDGET_CACHE[viewerName] = widget, layout
        widget, layout = self._VIEWER_WIDGET_CACHE[viewerName]

        try:
            layout.addWidget(widget)
        except RuntimeError:
            return

        return widget

    # FUNCTION
    # TEST IF WIDGET EXISTS IN CACHE DICTIONARY
    # --------------------------------------------------------------------------------------
    def widgetExists(self):
        try:
            widgetLayout = self._VIEWER_WIDGET_CACHE.get(self._viewerName)
            widgetLayout[0].parent()
        except RuntimeError:
            return False
        return True

    # FUNCTION
    # MEASURE BBOX SIZE WHETHER BBOX IS TOO LARGE OR NOT
    # --------------------------------------------------------------------------------------
    def check_bboxSize(self):
        widgetLayout = self._VIEWER_WIDGET_CACHE.get(self._viewerName)
        if not widgetLayout:
            return
        widget, layout = widgetLayout

        maxWidth = nuke.root().width() * 2
        maxHeight = nuke.root().height() * 2

        try:
            bbox = self.getbbox(self._viewer).bbox()
            bboxWidth = bbox.w()
            bboxHeight = bbox.h()

            if bboxWidth > maxWidth or bboxHeight > maxHeight:
                widget.setHidden(False)
            else:
                widget.setHidden(True)
        except:
            widget.setHidden(True)

# FUNCTION
# GET BBOX CHECKER INSTANCE TO RUN IT
# --------------------------------------------------------------------------------------
def get_bboxChecker(viewer):
    # global _BBOX_CHECKER
    bboxInstance = _BBOX_CHECKER.get(viewer.name())
    if not bboxInstance or not bboxChecker(viewer).widgetExists():
        bboxInstance = bboxChecker(viewer)
        _BBOX_CHECKER[viewer] = bboxInstance
    return bboxInstance

# FUNCTION
# EXECUTES ONLY IF VIEWER CHANGES INPUT
# --------------------------------------------------------------------------------------
def viewerUpdate():
    if nuke.activeViewer():
        viewer = nuke.activeViewer()
        if viewer.activeInput() == None:
            bboxInstance = bboxChecker(viewer.node())
            bboxInstance.check_bboxSize()
        else:
            run_bboxChecker()

# FUNCTION
# RUN BBOX CHECKER
# --------------------------------------------------------------------------------------
def run_bboxChecker():
    if nuke.activeViewer():
        viewer = nuke.activeViewer()
        getbboxInstance = get_bboxChecker(viewer.node())
        getbboxInstance.check_bboxSize()
    else:
        bboxInstance = bboxChecker(nuke.activeViewer())