import nuke
import sys

# change NukeScripts variable to directory the python file is located
NukeScripts = "C:\example"
if not NukeScripts in sys.path:
    sys.path.append(NukeScripts)

import BBox_Checker

nuke.addOnScriptLoad(BBox_Checker.run_bboxChecker)
nuke.addUpdateUI(BBox_Checker.run_bboxChecker)
nuke.addKnobChanged(BBox_Checker.viewerUpdate,  nodeClass = 'Viewer')